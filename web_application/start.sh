#!/bin/bash

# pip upgrade
pip install pip --upgrade

# installation of python dependencies
pip install -r requirements.txt

# make these variables as environment variables because they are necessary for the application
export FLASK_APP=flaskr
export FLASK_ENV=development

# create new database if it not exists (normally play the 1st time you deploy)
if [[ ! -f "./instance/flaskr.sqlite" ]]; then
    flask init-db
fi

# launching the flask web server to run the application
flask run --host=0.0.0.0
