
# Flask Blog CI-CD (Continuous Integration and Continuous Delivery) avec Gitlab CI-CD/Ansible/Docker/AWS EC2

## Présentation:
Ce projet permet de déployer une application web en [Python](https://www.python.org/), [Flask](https://flask.palletsprojects.com/en/1.1.x/), [Sqlite](https://www.sqlite.org/index.html) en utilisant les pipelines [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/), [Ansible](https://www.ansible.com/), [Docker](https://www.docker.com/) sur une instance [Amazon Web Services](https://aws.amazon.com/fr/ec2/) (EC2).  
L'application web en question est un simple blog, le but ici est surtout de comprendre le CI/CD avec les pipelines Gitlab CI/CD, le provisionning d'instance AWS (EC2) avec Ansible et la conteneurisation avec Docker/docker-compose.  
Il peut être adapté à d'autres applications mais plusieurs modifications seront à prévoir.

## Prérequis:
1. Un [compte Gitlab](https://gitlab.com/users/sign_up) pour cloner ce projet et pouvoir utiliser Gitlab CI/CD
2. Un [compte Amazon Web Services](https://aws.amazon.com/fr/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc), avec un utilisateur IAM ayant:
- un Accès par programmation
- une stratégie lui donnant les full access sur le service EC2 (AmazonEC2FullAccess)
- sauvegardez bien votre Access ID et Access Key, vous en aurez besoin pour la suite
- vous pouvez vous aider de cette [documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/security-iam.html)
1. Un [compte Docker Hub](https://hub.docker.com/)
2. Application Web (le blog): Il se trouve dans le dossier `web_application`. Je me suis beaucoup inspiré de ce [tutoriel](https://flask.palletsprojects.com/en/1.1.x/tutorial/). Faites-y un petit tour, cela est très intéressant.
3. Votre paire de clé ssh (**sans passphrase**). Pour savoir comment en créer une, cliquez [ici](https://www.ssh.com/ssh/keygen/)
4. [Installer Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) sur son PC
5. [Configurer](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) votre PC pour fonctionner avec votre compte Gitlab

## Démarrage:
### Explication:
Avant de lancer le projet, il est important de savoir ce qu'il fait un peu plus en détail:
1. Lorsque l'on push le projet sur Gitlab, cela va déclencher un pipeline Gitlab CI/CD, pourquoi? Car dans le projet il y a un fichier qui se nomme `.gitlab-ci.yml`, celui-ci indique à Gitlab qu'il y a un pipeline à lancer et lui indique aussi ce qu'il doit faire
2. C'est quoi un pipeline? C'est un genre de processus temporaire, sur Gitlab on parle de `runner`, c'est un conteneur dans lequel va se jouer les actions demandées dans le fichier `.gitlab-ci.yml`
3. Que fait ce pipeline? De manière générale un pipeline permet de faire beaucoup de choses mais dans notre cas il va faire 4 choses en 4 étapes et en respectant bien l'ordre de ces étapes. Quels sont ces étapes? 
   1. `build_push_custom_ansible_image`: Cela va créer une image personnalisée docker contenant Ansible qui sera ensuite "pushé" sur votre compte Docker Hub pour ensuite être utilisée à l'étape de création de l'instance EC2 d'AWS et pour l'étape de déploimement de l'application web sur cette instance
   2. `test`: Lance des tests unitaires pour tester notre application web
   3. `create_instance_aws_ec2`: Cette étape lance un playbook Ansible qui crée l'instance EC2 d'AWS en installant toutes applications nécessaires pour lancer des conteneurs Docker
   4. `deploy`: Dernière étape, elle va lancer un playbook Ansible qui va déposer l'application web sur l'instance EC2 d'AWS puis la lancer pour qu'elle soit opérationnelle via Docker

### Préparation:
+ [Cloner](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) ce projet Gitlab sur votre PC **SANS le push sur votre Gitlab** (sinon un pipeline va se déclencher alors que vous n'avez pas encore paramétré le projet)
+ Assurez-vous que votre fichier contenant votre clé ssh public se nomme bien `id_rsa.pub` et qu'il a les droits 644(sous Windows ces droits ne sont pas nécessaires) puis remplacez celle du projet qui se trouve dans le dossier `custom_docker_images/ansible/key_ssh/` par la votre
+ Il faut créer des [variables d'environnement dans votre projet Gitlab ](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) fraîchement cloné. Utiliser cette méthode permet de ne jamais transmettre en clair certaines données sensibles (ex: mot de passe, clé ssh, ...). Voici celles que vous devez paramétrer (laissez tout par défaut et remplissez juste `Key` et `Value`):

| Key | Value | Description |
| --- | ----- | ----------- |
| AWS_ACCESS_KEY_ID   | VOTRE_AWS_ACCESS_KEY_ID | Il s'agit de l'`Access Key ID` de votre utilisateur IAM ayant les accès sur le service EC2 (cf. "Prérequis 2"). Cela va vous permettre de lancer des actions sur le service EC2 d'AWS via les playbook Ansible |
| AWS_SECRET_ACCESS_KEY | VOTRE_AWS_SECRET_ACCESS_KEY | Il s'agit de l'`Access Secret Key ID` de votre utilisateur IAM ayant les accès sur le service EC2 (cf. "Prérequis 2"). Cela va vous permettre de lancer des actions sur le service EC2 d'AWS via les playbook Ansible |
| USER_DOCKER_HUB | VOTRE_DOCKER_ID | Il s'agit de votre `Docker ID` pour vous connecter à Docker Hub (cf. "Prérequis 3"). Il sera utile pour pusher une custom image dans Docker Hub puis cette image nous sera très utile pour lancer les playbooks d'Ansible et ainsi provisionner l'instance EC2 d'AWS |
| PASS_DOCKER_HUB | VOTRE_DOCKER_PASSWORD | Il s'agit de votre `Password` pour vous connecter à Docker Hub (cf. "Prérequis 3"). Il sera utile pour pusher une custom image dans Docker Hub puis cette image nous sera très utile pour lancer les playbooks d'Ansible et ainsi provisionner l'instance EC2 d'AWS |
| SSH_PRIVATE_KEY | CONTENU_DE_VOTRE_SSH_PRIVATE_KEY | Il s'agit de la `clé privé ssh` qui va permettre aux playbooks d'Ansible de communiquer avec l'instance EC2 d'AWS |

### Lancement:
Une fois l'étape de préparation terminée, vous pouvez maintenant lancer le déploiement de l'application. Pour cela, suivez les étapes suivantes :
```lang-bash
cd <PROJECT_DIRECTORY>
```
```lang-bash
git add .
```
```lang-bash
git commit -m "<MESSAGE_DE_VOTRE_COMMIT>"
```
```lang-bash
git push
```

C'est bientôt terminé !  
Retournez maintenant sur le `site de Gitlab -> Le projet -> CI/CD (menu à gauche) -> Pipelines`, vous devriez voir un pipeline se lancer. Il vous reste plus qu'à patienter quelques minutes...  
Une fois le pipeline terminée, rendez-vous sur le site d'`AWS -> Région Londres -> service EC2`, vous devriez voir une nouvelle instance EC2.  
Pour lancer la page internet contenant le blog il faut tapper dans l'URL l'adresse IP Public de votre instance EC2 (cette information se trouve facilement lorsque vous cliquez sur votre instance EC2 dans AWS et faites bien attention à mettre http et non https, le blog n'est pas très bien sécurisé mais ce n'est pas le but de ce projet).  
Vous voilà ENFIN sur le blog, BRAVO !

### Troubleshooting:
En cas de problème vous le verrez assez rapidement dans le pipeline Gitlab CI/CD, les messages d'erreurs sont plutôt explicites.
