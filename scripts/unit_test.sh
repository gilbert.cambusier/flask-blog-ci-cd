#!/bin/bash

# Move in the app folder
cd web_application

# Upgrade pip
pip install pip --upgrade

# Install necessary python dependencies for the app
pip3 install -r requirements.txt

# Launch unit test
coverage run -m pytest
